from django.db import models
from django.utils.translation import gettext_lazy as _

class Tax_rate(models.Model):
    number = models.IntegerField(
        verbose_name=_("number"),
        null=False, blank=True,
        )
    name = models.CharField(
        max_length=120,
        verbose_name=_("name"),
        null=False, blank=True,
        )
    rate_of_tax = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=_("tax rate"),
        null=True, blank=False,
        )

class Commodity_group(models.Model):
    number = models.IntegerField(
        verbose_name=_("number"),
        null=False, blank=False,
        db_index=True,
        )
    name = models.CharField(
        max_length=120,
        verbose_name=_("name"),
        null=False, blank=False,
        )
    tax_rate = models.ForeignKey(
        Tax_rate, on_delete=models.SET_NULL, null=True,
        )
    

class Account(models.Model):
    number = models.IntegerField(
        verbose_name=_("account number"),
        null=False, blank=False,
        )
    name = models.CharField(
        max_length=120,
        verbose_name=_("account name"),
        null=False, blank=False,
        )
    commodity_group = models.IntegerField(
        verbose_name=_("commodity group"),
        null=False, blank=True,
        )
    valid_from = models.DateField(
        verbose_name=_("valid from"),
        null=True, blank=False,
        )
    valid_until = models.DateField(
        verbose_name=_("valid until"),
        null=True, blank=False,
        )


class Journal(models.Model):
    amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name=_("amount"),
        null=False, blank=False,
        )
    tax_amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name=_("tax amount"),
        null=True, blank=True,
        )
    net_amount = models.DecimalField(
        max_digits=100,
        decimal_places=2,
        verbose_name=_("net amount"),
        null=True, blank=True,
        )
    posting_key = models.IntegerField(
        verbose_name=_("posting key"),
        null=True, blank=True,
        )
    debit_account = models.IntegerField(
        verbose_name=_("debit account"),
        null=False, blank=False,
        )
    credit_account = models.IntegerField(
        verbose_name=_("credit account"),
        null=False, blank=False,
        )
    date_field = models.DateField(
        verbose_name=_("date field"),
        null=False, blank=False,
        )
    posting_text = models.CharField(
        max_length=120,
        verbose_name=_("posting text"),
        null=False, blank=False,
        )
    voucher_1 = models.CharField(
        max_length=25,
        verbose_name=_("voucher-1"),
        null=True, blank=True,
        )


class Posting_key(models.Model):
    tax_key = models.IntegerField(
        verbose_name=_("tax key"),
        null=True, blank=False,
        )
    name = models.CharField(
        max_length=120,
        verbose_name=_("name"),
        )
    valid_from = models.DateField(
        verbose_name=_("date field"),
        null=True, blank=True,
        )
    active_flag = models.BooleanField(
        null=False, blank=True,
        )
    tax_rate = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        verbose_name=_("tax rate"),
        null=True, blank=True,
        )

