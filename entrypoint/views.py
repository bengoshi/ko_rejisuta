from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, permission_required

from .models import Tax_rate,\
    Commodity_group,\
    Account,\
    Journal,\
    Posting_key

def journal(request):
    journal = Journal.objects.all()
    context = {"journal": journal}
    return render(request, "entrypoint/journal.html", context)

def settings(request):
    pass


def cashRegister(request):
    if request.method == "POST":
        form = CashRegisterForm(request.POST or None, initial='')
        if form.is_valid():
            form.save()
            form = CashRegisterForm(initial='')
        return redirect("/cashRegister")
    else:
        form = CashRegisterForm(initial='')
    context = {'form': form}
    return render(request, "entrypoint/cashRegister.html", context)

